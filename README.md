Hello, to get started do the following

1. Clone the repo to on a computer with Node installed.
2. In a terminal (the computer must also have a terminal) run 'npm install'
3. Make a coffee.
4. Run 'npm run start'

The thing should load
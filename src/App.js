import React from 'react';
import './App.scss';
import { loadData, processData } from './dataHandling'
import { ReactGraph } from './GraphElements/ReactGraph'
import { RegionSelector } from './RegionSelector'

class App extends React.Component {

  constructor() {
    super()
    
    this.state = {
      loading: true,
      data: null,
      selectedRegion: null,
      regions: []
    }
    
    this.init() 
  }

  init() {
    
    loadData().then((data) => {
      
      const regions = processData(data)
      
      this.setState({
        data, 
        regions, 
        selectedRegion: regions.items[regions.keys[0]], 
        selectedDatum: null,
        loading: false
      })

    })

  }

  onRegionSelect = (event) => {
    const { regions } = this.state
    const selectedRegion = regions.items[event.target.value]
    this.setState({ selectedRegion, selectedDatum: null })
  }

  onDatumSelect = (datum) => {
    this.setState({ selectedDatum: datum })
  }

  renderSelectedDatum () {
    const { selectedDatum } = this.state
    if (selectedDatum) {
      const { week, upper, mean, low } = selectedDatum
      return (
        <div className="readout container">
          <p className="title"><b>Week { week } Yield Estimate Range</b></p>
          <p>Upper: <b>{ upper.toFixed(1) }</b></p>
          <p>Mean: <b>{mean.toFixed(1)}</b></p>
          <p>Lower: <b>{low.toFixed(1)}</b></p> 
        </div> 
      )
    }
  }

  renderLoading () {
    return (
      <div className="app">
        <div className="title container">Loading...</div>
      </div>
    )
  }
  
  render () {
    const { regions, selectedRegion, selectedDatum, loading } = this.state

    // Can't show much unless the data is loaded
    if (loading) {
      return this.renderLoading()
    }

    // Create the list of regions to select
    const { keys } = regions
    const regionList = keys.map((key) => {
      return { name: regions.items[key].name, id: key }
    })
    
    return (
      <div className="App">
        <div className="title container">Onion yield estimates (t/ha) {selectedRegion.name} {selectedRegion.year}</div>
        
        <div className="graph container">
          <ReactGraph 
            selectedRegion={ selectedRegion } 
            width={ 1280 - 2 } 
            height={ 800 - 2 }
            startWeek={ regions.startWeek }
            endWeek={ regions.endWeek }
            min={ regions.min }
            max={ regions.max }
            onHover={ this.onDatumSelect }
          />
        </div>

        <div className="controls container">
          <RegionSelector 
            onChange={ this.onRegionSelect } 
            regionList={ regionList  }/>
        </div> 
        
        { this.renderSelectedDatum() }

      </div>
    );
  }
  
}

export default App;

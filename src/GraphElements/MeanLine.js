import React from 'react'
import { line } from 'd3'
import { datumToPoint } from './util'

export const MeanLine = ({ selectedRegion, scaleX, scaleY, onHover }) => {

  const { data } = selectedRegion

  const points = data.map(d => datumToPoint(d, 'mean', scaleX, scaleY))
  const pathGen = line().x(d => d.x).y(d => d.y)
  const pathStr = pathGen(points)
  
  const dataHighlights = data.map((d, i) => {
    const position = points[i]
    return <circle 
              cx={position.x} 
              cy={position.y}
              key={d.week}
              r="20" 
              className="data-highlight"
              onMouseOver={() => onHover(d) }
              // onMouseOut={() => onHover(null) }
            /> 
  })

  return (
    <>
      <path d={pathStr} className="mean-line"/>
      { dataHighlights }
    </>
  )



}

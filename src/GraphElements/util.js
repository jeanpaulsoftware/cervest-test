export const transformToString = (x, y) => `translate(${x}, ${y})`

export const datumToPoint = (datum, key, scaleX, scaleY) => {
  return { x: scaleX(datum.week), y: scaleY(datum[key]) }
}
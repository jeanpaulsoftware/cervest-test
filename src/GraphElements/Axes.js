import React from 'react'
import { transformToString } from './util' 

export const LeftAxis = ({ scaleX, scaleY, padding }) => {

  const ticks = []
  const lineOffset = 30
  const textOffsetY = 5  
  const values = scaleY.ticks(10)

  values.forEach((value, index) => {
    const width = scaleX.range()[1]
    const y = scaleY(value)

    ticks.push(
      <YTick 
        width={width} 
        y={y} 
        lineOffset={lineOffset}
        textOffsetY={textOffsetY} 
        value={value}
        key={value}
      />
    )
  })

  return (
    <g transform={transformToString(padding, 0)}>
      { ticks }
    </g>
  )

}

export const BottomAxis = ({ scaleX, scaleY, padding, y }) => {

  const ticks = []
  const values = scaleX.ticks(5)
  const textOffsetY = 50
  
  const yTicks = scaleY.ticks()
  const min = scaleY(yTicks.shift())
  const max = scaleY(yTicks.pop())

  const tickHeight = (max - min)
  
  
  values.forEach((value) => {
    ticks.push(
      <XTick 
        key={ value } 
        value={ value } 
        textOffsetY={ textOffsetY }
        x={ scaleX(value) }
        tickHeight={ tickHeight }
      />
    )
  })

  return (
    <g transform={transformToString(padding, y)}>
      { ticks }
    </g>
  )
}

const XTick = ({x, value, tickHeight, textOffsetY}) => {

  return (
    <g transform={transformToString(x, 0)}>
      <line x1="0" x2="0" y1={textOffsetY / 4} y2={tickHeight} className="axis-tick" />
      <text transform={transformToString(0, textOffsetY)} className="bottom-axis-label">
        Week { String(value) }
      </text>
    </g>
  )

}

const YTick = ({width, lineOffset, textOffsetY, y, value }) => {
  return (
    <g transform={`translate(0, ${y})`}>
      <text transform={`translate(0, ${textOffsetY})`}>{value}</text>
      <line className="axis-tick"
        x1={lineOffset} 
        x2={width} 
        y1={0} 
        y2={0}
      />
    </g>
  )

}
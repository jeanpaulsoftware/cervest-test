import React from 'react'
import { line } from 'd3'

import { transformToString, datumToPoint } from './util' 

// Accessor values in this are hardcoded, but could be parameterised
const WormSegment = ({scaleX, scaleY, d0, d1}) => {

  // Create a shape going clockwise from the top left
  const p1 = datumToPoint(d0, 'upper', scaleX, scaleY)
  const p2 = datumToPoint(d1, 'upper', scaleX, scaleY)
  const p3 = datumToPoint(d1, 'low', scaleX, scaleY)
  const p4 = datumToPoint(d0, 'low', scaleX, scaleY)

  const pathGen = line().x(d => d.x).y(d => d.y)
  const pathStr = pathGen([p1, p2, p3, p4])

  return <path d={pathStr} className="worm"/>
  
}

export const Worm = ({selectedRegion, scaleX, scaleY, padding}) => {

  const { data } = selectedRegion
  const segments = []

  for (var i = 0; i < data.length -1; i++) {

    let d0 = data[i]
    let d1 = data[i + 1]
    segments.push(
      <WormSegment 
        key={i}
        d0={d0}
        d1={d1}
        scaleX={scaleX}
        scaleY={scaleY}
      />
    )

  }

  return (
    <g transform={transformToString(padding, padding)}>
      {segments}
    </g>
  )

}
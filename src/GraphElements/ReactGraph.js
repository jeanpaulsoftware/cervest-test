import * as d3 from 'd3'
import React, { Component } from 'react'

import { transformToString } from './util'
import { LeftAxis, BottomAxis } from './Axes'
import { Worm } from './Worm'
import { MeanLine } from './MeanLine'

export class ReactGraph extends Component {

  padding = 50
  scaleX = null
  scaleY = null
  svg = null
  mounted = false

  setScales = () => {
    
    const { width, height, startWeek, endWeek, min, max } = this.props
    const { padding } = this
    
    this.scaleX = d3.scaleLinear()
                    .domain([startWeek - 1, endWeek + 1])
                    .range([padding, width - padding - padding])
                    
    this.scaleY = d3.scaleLinear()
                    .domain([min - 10, max + 10])
                    .range([height - padding - padding, padding])
    
  }

  render () {
    
    this.setScales()

    const { width, height, selectedRegion, onHover } = this.props
    const { scaleX, scaleY, padding } = this
    const bottomAxisY = scaleY(scaleY.ticks()[0])

    return (
      <svg width={ width } height={ height }>
        <LeftAxis padding={padding} scaleX={scaleX} scaleY={scaleY}/>
        <BottomAxis padding={padding} scaleX={scaleX} scaleY={scaleY} y={bottomAxisY} />
        <Worm 
          padding={padding} 
          scaleX={scaleX} 
          scaleY={scaleY} 
          selectedRegion={selectedRegion} 
        />
        <g transform={transformToString(padding, padding)}>
          <MeanLine 
            selectedRegion={selectedRegion}
            scaleX={scaleX}
            scaleY={scaleY}
            onHover={onHover} 
          />
        </g>

      </svg>
    )
    

  }

}
import React from 'react'

export const RegionSelector = ({ regionList, onChange }) => {
  
  const regionOptions = regionList.map((region) => {
    const {id, name} = region
    return <option key={id} value={id}>{name}</option>
  })

  return (
    <select onChange={onChange}>
      {regionOptions}
    </select>
  )
  

}
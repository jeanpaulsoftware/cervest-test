import * as d3 from 'd3'
import { Component } from 'react';

export class Graph extends Component {

  componentDidMount () {
    this.mounted = true
    this.draw()
  }

  componentDidUpdate() {
    if (this.mounted) {
      // Bit crude but it'll do for now
      d3.select('svg').remove()
      this.draw()
    }
  }

  padding = 20
  scaleX = null
  scaleY = null
  svg = null
  mounted = false

  draw () {
    
    const { 
      graphId, 
      width, 
      height, 
      startWeek,
      endWeek,
      min,
      max 
    } = this.props

    const { padding } = this

    this.scaleX = d3.scaleLinear()
                    .domain([startWeek - 1, endWeek + 1])
                    .range([padding, width - padding * 2])
                    
    this.scaleY = d3.scaleLinear()
                    .domain([min - 10, max + 10])
                    .range([height - padding * 2, padding])
    
    this.svg = d3.select('#' + graphId)
                  .append('svg')
                  .attr("width", width)
                  .attr("height", height)

      this.drawAxes()
      this.drawMeans()
      this.drawWorm()
  }

  drawAxes () {

    const { padding } = this
    const { height } = this.props
    
    

    const bottomAxis = d3.axisBottom().scale(this.scaleX)
    this.svg.append('g')
            .attr('transform', `translate(${padding}, ${ height - padding * 2 })`)
            .call(bottomAxis)

    const leftAxis = d3.axisLeft(this.scaleY)
    this.svg.append('g')
            .attr('transform', `translate(${ padding * 2})`)
            .call(leftAxis)

  }

  drawMeans () {
    const { scaleX, scaleY, svg } = this
    const { selectedRegion } = this.props
    const { data, mean } = selectedRegion

    let startWeek = Number.POSITIVE_INFINITY
    let endWeek = Number.NEGATIVE_INFINITY

    data.forEach((datum) => {
      startWeek = Math.min(startWeek, datum.week)
      endWeek = Math.max(endWeek, datum.week)
    })

    const x0 = scaleX(startWeek)
    const x1 = scaleX(endWeek)
    const y = scaleY(mean)
    
    svg.append('g')
        .append('path')
        .attr('d', `M${x0} ${y} L${x1} ${y}`)
        .attr('class', 'global-mean')

  }

  drawWorm () {

    const { selectedRegion } = this.props
    const { data } = selectedRegion
    const {scaleX, scaleY} = this

    let addLine = (pathString, className) => {
      this.svg.append('g')
            .append('path')
            .attr('d', pathString)
            .attr('class', className)
    }

    const upperGenerator = d3.line().x((d) => scaleX(d.week))
                                    .y((d) => scaleY(d.upper))
    
    const meanGenerator = d3.line().x((d) => scaleX(d.week))
                                    .y((d) => scaleY(d.mean))
    
    const lowerGenerator = d3.line().x((d) => scaleX(d.week))
                                    .y((d) => scaleY(d.low))

    addLine(upperGenerator(data), 'upper')
    addLine(meanGenerator(data), 'mean')
    addLine(lowerGenerator(data), 'lower')

  }



  render () {
    return null
  }

}
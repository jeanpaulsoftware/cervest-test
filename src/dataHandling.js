import { csv } from 'd3'

const filenames = [
  './data/1-20th_June.csv',
  './data/2-10th_July.csv',
  './data/3-26th_July.csv',
  './data/4-16th_August.csv',
  './data/5-30th_August.csv'
]

export async function loadData() {

  const promises = []
  
  for (let i = 0; i < filenames.length; i++) {
    const file = filenames[i]
    promises.push(await csv(file))
  }

  return Promise.all(promises)

}

export const processData = (data) => {

  const filenames = Object.keys(data)
  
  /* 
  So we need all of the following for the graph
  - min and max for scale
  - a list of items
  - a list of item names
  */
  const allRegions = {
    min: Number.POSITIVE_INFINITY,
    max: Number.NEGATIVE_INFINITY,
    startWeek: Number.POSITIVE_INFINITY,
    endWeek: Number.NEGATIVE_INFINITY,
    keys: [],
    items: {}
  }

  // Unfortunately we need to do a lot of work because the data
  // isn't in a great format. TypeScript would be very useful
  // here, but also much more time consuming to write- particularly
  // with D3
  
  filenames.forEach((file) => {
    const rawItems = data[file]
    rawItems.forEach((rawItem) => {
      
      // First rename the variables to be a bit more sensible
      const {
        NUTS_ID, 
        RegionName: name, 
        confidence_level: confidence, 
        prediction_lower: low,
        prediction_mean: mean,
        prediction_upper: upper,
        week,
        year
      } = rawItem
      
      // If we've not seen this region before, create a new entry for it
      if (!allRegions.items[NUTS_ID]) {
        allRegions.items[NUTS_ID] = {
          data: [],
          id: NUTS_ID,
          name,
          mean: 0,
          year
        }
        allRegions.keys.push(NUTS_ID)
      }
      
      // Add the entry
      allRegions.items[NUTS_ID].data.push({
        confidence: Number(confidence), 
        low: Number(low), 
        mean: Number(mean), 
        upper: Number(upper), 
        week: Number(week), 
        year
      })
      
      allRegions.items[NUTS_ID].mean += Number(mean)
       
    })
    
  })

  // Loop through again and compute the mean, min and max
  Object.keys(allRegions.items).forEach((id) => {
    const region = allRegions.items[id]
    const { data } = region
    
    // I'm going to assume this is required
    data.sort((a, b) => a.week > b.week)
    
    region.mean /= data.length
    
    data.forEach((datum) => {

      const { low, upper, week } = datum

      allRegions.min = Math.min(low, allRegions.min)
      allRegions.max = Math.max(upper, allRegions.max)
      allRegions.startWeek = Math.min(week, allRegions.startWeek)
      allRegions.endWeek = Math.max(week, allRegions.endWeek)

    })
    
  })

  return allRegions

}